### Introducción
Este proyecto de Spring Boot con Spring Security tiene como objetivo implementar la inserción
en la base de datos de autenticación de los clientes que pueden consumir las rutas del servicio REST  

Para importar el proyecto basta con clonarlo desde el repositorio y abrirlo con el IDE de nuestro agrado  
Se utiliza una base de datos mysql con el nombre db_springboot_backend ((Se puede modificar en el archivo application.propertis de la raíz del sitio))

###Características implementadas
####Spring Boot:  
* Script de inserción de datos de ejemplo
* CRUD de una entidad (Cliente) en base de datos mediante API REST 
####Spring security:  
- Backend Spring Boot con Spring security
- Autentificaciòn Oauth
- Inserción de clientes Oauth en base de datos (Clientes dinámicos)
- Control de acciones mediante Roles (ACL)
- Generación de tokens JWT para autenticación de clientes de la API
- Encripción, desencripción, validación de Tokens JWT con algoritmo RS256
###Librerías adicionales   
- Project Lombok (Anotaciones para reducir codigo boilerplate)
###Extras
- Hay una colección Postman disponible para realizar pruebas en el enlace https://www.getpostman.com/collections/e73dd7db0c40aa70c6df
###Pendientes de implementación:   
- Swagger
- Interfaz Swagger HTML
- Projections
- Repositorios con método para búsqueda de entidades
- API de ejemplo de facturación
###Spring Data